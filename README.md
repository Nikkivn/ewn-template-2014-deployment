# EWN 2014 template - deployment build

Source and Deployment build for EWN Templates 2014!

## Requirements

You'll need to have the following items installed before continuing.

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Grunt](http://gruntjs.com/): Run `[sudo] npm install -g grunt-cli`
  * [Bower](http://bower.io): Run `[sudo] npm install -g bower`

## Quickstart

```bash
git clone https://bitbucket.org/barryroodt/ewn-template-2014-deployment/
```

```bash
cd ewn-template-2014-deployment/
```

```bash
npm install && bower install
```

While you're working on your project, run:

`grunt`

To build a 'production' ready version (unminified JS), run:

`grunt publish`

To build a 'production' ready version WITH minified JS, run:

`grunt publish --minify`

And you're set!

## Directory Structure
  * `app/` : main source folder
  * `app/scss/_settings.scss`: Foundation configuration settings go in here
  * `app/scss/app.scss`: Application styles go here
  * `app/js/custom`: Custom plugins written specifically for EWN
  * `app/js/app.js`: Main app JS file. Insert custom JS here if required.
  * `app/js/app-ie.js`: IE8 specified JS. Insert custom IE8 specific JS here.
  * `app/js/vendor`: Vendor plugins.
  * `app/js/ie`: IE8 specific polyfills.
  * `app/img/mediaelement`:  SWF, Silverlight, PNG files specific to media element player.

## Important Notes

Only edit source files in the `app/` folder. Changes made here are tracked by the repository.


```>
grunt publish
```

will create/overwrite a `production/` folder which contains the deployment ready code.

Changes to anything inside `production/` will **NOT** be tracked by the repository.