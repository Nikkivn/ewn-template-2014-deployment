;
/*!
 * weatherwidget.js
 */
(function ($, window, document, undefined) {
    var pluginName = 'weatherWidget',
        defaults = {
            active_class: 'show',
            inactive_class: 'hide'
        };

    function WeatherWidget(element, options) {
        this.options = $.extend({}, defaults, options);
        this.element = $(element);
        this.selector = this.element.find('select');
        this.selected = this.selector.find(':selected');
        this.weatherContent = this.element.find('.weather-content');
        this.currentContent = this.element.find('.weather-current');
        this.currentIcon = this.currentContent.find('i');
        this.currentHi = this.currentContent.find('.hi-temp');
        this.currentLo = this.currentContent.find('.lo-temp');
        this.menuItem = $('a[data-menu-content="mega-weather"]');
        this.menuItemIcon = this.menuItem.find('i');

        this.init();
    }

    WeatherWidget.prototype.init = function () {
        var self = this;
        this.show();

        this.selector.on('change', function(){
            self.show();
        })
    };

    WeatherWidget.prototype.show = function(){
        this.hide();
        var city = this.selector.val();
        var $content = $('#weather-data-'+city);
        if (!$content.length){
            $content = $(this.weatherContent[0]);
        }

        var $today = $content.find('ul.forecast.today');
        var icon = $today.find('i').data('icon');
        var hitemp = $today.find('li.hi-temp').text();
        var lotemp = $today.find('li.lo-temp').text();

        this.currentIcon.attr('data-icon', icon);
        this.currentHi.text(hitemp);
        this.currentLo.text('/ ' + lotemp );

        //this.menuItemIcon.attr('data-icon', icon);
        this.menuItem.html('<i data-icon="'+icon+'"></i>' +hitemp);




        $content
            .removeClass(this.options.inactive_class)
            .addClass(this.options.active_class);


    };

    WeatherWidget.prototype.hide = function(){
        this.weatherContent
            .removeClass(this.options.active_class)
            .addClass(this.options.inactive_class);
    };




    // handle multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName,
                    new WeatherWidget(this, options));
            }
        });
    };
})(jQuery, window, document);