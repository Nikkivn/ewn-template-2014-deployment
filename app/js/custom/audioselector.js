;
/*!
 * audioselector.js
 */
(function ($, window, document, undefined) {
    var pluginName = 'audioSelector',
        defaults = {
            width: 152,
            height: 25,
            features: ['playpause', 'progress'],
            path: window.location.protocol + '//' + window.location.host + '/img/mediaelement/',
            rollUrl: '',
            pre_postRoll: '',
            audioUrl: ''
        };

    function AudioSelector(element, options) {
        this.element = element;
        this.selectElement = $(this.element).find('select');
        this.audioElement = $(this.element).find('audio');
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this._allAudioElements = $('audio');
        this.player = {};

        this.init();
    }

    AudioSelector.prototype.init = function () {
        var self = this;
        //this.selectElement.selectBox();  // assign the select box an instance of the 'selectBox()' plugin.
        $(this.audioElement).mediaelementplayer({
            pluginPath: this.options.path,
            audioWidth: this.options.width,
            audioHeight: this.options.height,
            features: this.options.features,
            mode: 'auto_plugin',
            success: function (player, node) {
                self.player = player;
                self.registerSelectChangeEvent();
            },
            error: function(e){
                console.log('error', e);
            }

        });


    };

    AudioSelector.prototype.registerSelectChangeEvent = function () {
        var self = this;
        this.selectElement.on('change', function () {
            var selected = $(this).find(':selected');
            self.updateOptions(selected.data());
            self.play()
        });
    };

    AudioSelector.prototype.updateOptions = function (data) {
        this.options.rollUrl = data.rollurl;
        this.options.audioUrl = data.audiourl;
        this.options.pre_postRoll = data.pre_postroll;
    };

    AudioSelector.prototype.stopAll = function () {
        var self = this;
        this._allAudioElements.each(function () {
            var el = $(this)[0];
            if (self.isPlayer(el)) {
                el.player.pause();
            }
        });
    };

    AudioSelector.prototype.isPlayer = function (obj) {
        return ((typeof obj != 'undefined') && (typeof obj.player != 'undefined') && obj.player);
    };

    AudioSelector.prototype.play = function () {
        var self = this;
        self.stopAll(); // first we stop all other instances playing.

        this.player.setSrc(self.options.audioUrl);
        this.player.load();
        this.player.play();

        /*

         if ((typeof(this.options.pre_postRoll) == "undefined" || this.options.pre_postRoll == "") || (typeof(this.options.rollUrl) == "undefined" || this.options.rollUrl == "")) {
         this.player.setSrc(self.options.audioUrl);
         this.player.load();
         this.player.play();
         }
         else {
         this.player.setSrc((self.options.pre_postRoll == "preRoll") ? self.options.rollUrl : self.options.audioUrl);


         var played = true;
         // add event listener
         this.player.addEventListener('ended', function (e) {

         //keep the ended event from infinitely looping the audio
         if (!played) {
         self.player.setSrc((self.options.pre_postRoll == "preRoll") ? self.options.audioUrl : self.options.rollUrl);

         played = true;
         self.player.load();
         self.player.play();
         }
         else {
         //if both pre/post roll and the actual audio playback has ended (played is true) then reset
         played = false;
         self.player.setSrc((self.options.pre_postRoll == "preRoll") ? self.options.rollUrl : self.options.audioUrl);
         //setTimeout("player.load();", 250);
         self.player.load();
         self.player.play();

         }
         //document.getElementById('current-time').innerHTML = mediaElement.currentTime;

         }, false);
         this.player.load();
         this.player.play();
         }

         */
    };
    // handle multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName,
                    new AudioSelector(this, options));
            }
        });
    }
})(jQuery, window, document);