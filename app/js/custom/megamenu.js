;
/*!
 * megamenu.js
 */
(function ($, window, document, undefined) {
    var pluginName = 'megaMenu',
        defaults = {
            menuItemName: 'mega-menu',
            menuContentName: 'div.mega-menu-content',
            dropdownItemName: 'mega-dropdown',
            dropdownContentName: 'div.mega-menu-dropdown-content',
            active_class: 'show',
            inactive_class: 'hide'
        };

    function MegaMenu(element, options) {
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.element = $(element);
        this.menuItems = this.element.find('li.'+ this.options.menuItemName+ ' a');
        this.menuContent = $(this.options.menuContentName);
        this.dropdownContent = $(this.options.dropdownContentName);
        this.dropdownItems = this.element.find('li.' + this.options.dropdownItemName + ' a');
        this.closeButton = $('a.mega-menu-close');
        this.selectedClass = 'selected';
        this.topBar = this.element.find('nav.top-bar');
        this.topBarHeight = this.topBar.height();
        this.topBarWidth = this.topBar.width();
        this.topBarMargin = parseInt(this.topBar.css('margin-left'));
        this.topBarPosition = {top: 0, left: 0};
        this.topPosition = 0;
        this.leftPosition = 0;
        this.timer = null;
        this.itemoffset = 0;
        this.isIE = this.isIETest(8, 'lte');

        this.init();
    }

    MegaMenu.prototype.init = function () {
        var self = this;

        self.calculate_position();

        this.menuItems.not(this.dropdownItems).on('click', function(e){
            e.stopPropagation();
            e.preventDefault();
            self.toggle($(this));
        });

        this.dropdownItems.on('click', function(e){
            e.stopPropagation();
            e.preventDefault();
            self.toggle($(this));
        });

        this.closeButton.on('click', function(e){
            e.stopPropagation();
            e.preventDefault();
            self.closeall();
        });

        // Position the Mega dropdowns
        $(window).on("load resize scroll", function () {
            //self.closeall();
            self.calculate_position();
        });

        $(window).on('scroll', function(){
            self.closeall(true);
        });
    };

    MegaMenu.prototype.calculate_position = function(){
        this.topBarPosition = (this.topBar.length) ? this.topBar.position() : {top: 0, left: 0};
        this.topPosition = this.topBarPosition.top + this.topBarHeight;
        this.leftPosition = this.topBarPosition.left + this.topBarMargin;
    };

    MegaMenu.prototype.toggle = function(target) {
        var content = $('#' + target.data('menu-content'));
        var targetPosition = target.position();

        if (content.length === 0) {
            return;
        }

        if (!content.hasClass(this.options.active_class)){
            this.closeall();
            if (!this.isIE){
                this.addCssRule(this.options.menuContentName +':before', {'left' : (targetPosition.left + ((target.outerWidth() / 2) - 8)) + 'px'});
            }
            this.open(content, target);
        } else {
            target.parent().removeClass(this.selectedClass);
            this.close(content);
        }
    };

    MegaMenu.prototype.close = function(dropdown) {
        var self = this;
        dropdown.each(function(){
            $(this).css('left', '-99999px')
                .addClass(self.options.inactive_class)
                .removeClass(self.options.active_class)
                .off('clickoutside touchstartoutside');
        });
        $(window).off("resize.megamenu");


    };

    MegaMenu.prototype.closeall = function (onScroll) {
        var self = this;
        var scrolling = onScroll || false;
        var isDropdownActive = false;

        this.dropdownItems.each(function () {
            if ($(this).parent().hasClass(self.selectedClass)){
                isDropdownActive = true;
            }
        });


        if (scrolling !== true) {
            this.dropdownItems.each(function () {
                $(this).parent().removeClass(self.selectedClass);
            });
            this.close(this.dropdownContent);

        }

        if (scrolling !== true || !isDropdownActive) {
            this.menuItems.each(function () {
                $(this).parent().removeClass(self.selectedClass);
            });
            this.close(this.menuContent);
            $(document).off('click.megamenu, touchend.megamenu');
        }


        $(window).off("resize.megamenu");

        if (typeof (initEWNArticleClick) == "function") {
            initEWNArticleClick(true);
        }
    };

    MegaMenu.prototype.open = function (content, target) {
        var self = this;
        var isDropdown = (typeof(target) !== 'undefined')? target.parent().hasClass(this.options.dropdownItemName) : false;
        //Foundation.libs.dropdown.closeall();

        if (this.timer){
            clearTimeout(this.timer);
        }

        if (this.element.hasClass('fixed')){
            content.css('position', 'fixed');
        } else {
            content.css('position', 'absolute');
        }

        target.parent().addClass(this.selectedClass);

        content
            .addClass(this.options.active_class)
            .removeClass(this.options.inactive_class)
            .css('top', this.topPosition);
        /*.one('clickoutside touchstartoutside', function(){
         self.closeall();
         });*/
        $(document).on('click.megamenu, touchend.megamenu', function(event) {
            if(!$(event.target).closest(content).length) {
                event.preventDefault();
                self.closeall();
            }
        });

        if (typeof (initEWNArticleClick) == "function"){
            initEWNArticleClick(false);
        }

        $(window).off("resize.megamenu").on("resize.megamenu", function () {
            self.calculate_position();
            self.set_position(content, target, isDropdown);
        });

        this.set_position(content, target, isDropdown);

        /*
         .off('mouseenter mouseleave')
         .on('mouseenter', function(){
         self.open($(this), target);
         })
         .on('mouseleave', function(e){
         if(e.target.tagName.toLowerCase() != "select") {
         self.close($(this));
         }
         })
         */



    };

    MegaMenu.prototype.set_position = function(content, target, isDropdown){
        if (!isDropdown){
            if (!this.isIE){
                content.css('left', this.topBar.offset().left)
                    .css('min-width', this.topBarWidth);
            } else {
                content.css('left', this.topBar.offset().left)
                    .css('top', (this.topPosition + 3))
                    .css('width', this.topBarWidth);
            }
        } else {
            content.css('left', ((target.offset().left - content.outerWidth()) + target.outerWidth()));
        }
    };

    /*!
     * jquery.addrule.js 0.0.1 - https://gist.github.com/yckart/5563717/
     * Add css-rules to an existing stylesheet.
     *
     * @see http://stackoverflow.com/a/16507264/1250044
     *
     * Copyright (c) 2013 Yannick Albert (http://yckart.com)
     * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php).
     * 2013/05/12
     **/

    MegaMenu.prototype.addCssRule = function (selector, styles, sheet) {
        if (this.isIE){
            return this;
        }
        styles = (function (styles) {
            if (typeof styles === "string"){
                return styles;
            }
            var clone = "";
            for (var p in styles) {
                if (styles.hasOwnProperty(p)) {
                    var val = styles[p];
                    p = p.replace(/([A-Z])/g, "-$1").toLowerCase(); // convert to dash-case
                    clone += p + ":" + (p === "content" ? '"' + val + '"' : val) + "; ";
                }
            }
            return clone;
        }(styles));
        sheet = sheet || document.styleSheets[document.styleSheets.length - 1];

        if (sheet.insertRule) {
            sheet.insertRule(selector + " {" + styles + "}", sheet.cssRules.length);
        } else if (sheet.addRule) {
            sheet.addRule(selector, styles);
        }

        return this;

    };

    MegaMenu.prototype.isIETest = function(version, comparison) {
        var cc      = 'IE',
            b       = document.createElement('B'),
            docElem = document.documentElement,
            isIE;

        if(version){
            cc += ' ' + version;
            if(comparison){ cc = comparison + ' ' + cc; }
        }

        b.innerHTML = '<!--[if '+ cc +']><b id="iecctest"></b><![endif]-->';
        docElem.appendChild(b);
        isIE = !!document.getElementById('iecctest');
        docElem.removeChild(b);
        return isIE;
    }


    // handle multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName,
                    new MegaMenu(this, options));
            }
        });
    };
})(jQuery, window, document);