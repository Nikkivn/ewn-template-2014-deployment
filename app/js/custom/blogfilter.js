$(function(){
    $btnAllArticles = $('a.filter-full-article');
    $btnHighlights = $('a.filter-highlights');
    //$allArticles = $('article.full-post');
    //$highlightedArticles = $('article.highlight');

    $btnAllArticles.on('click', function(e){
        e.preventDefault();
        //$allArticles.show();
        $('article.full-post').show();
        $(this).addClass('active');
        $btnHighlights.removeClass('active');
    });

    $btnHighlights.on('click', function(e){
        e.preventDefault();
        //$allArticles.hide();
        $('article.full-post').hide();
        //$highlightedArticles.show();
        $('article.highlight').show();
        $(this).addClass('active');
        $btnAllArticles.removeClass('active');
    });
});