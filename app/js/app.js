/*!
 * app.js
 */

$(window).load(function() {
    $(document).foundation();

    if (!ewnIsIETest(8, 'lte')){
        $('.on-air select, .display-filter select, .category-filter select, .city-select select').customSelect({customClass: 'custom-select'});
    }

    $("#NewsBulletinAudio, #TrafficBulletinAudio").audioSelector({
        width: 150,
        height: 25
    });
    var $container = $('#container');
    if ($container.length){
        $container.imagesLoaded(function(){
            $container.masonry({
                itemSelector: '.box',
                isAnimated: !Modernizr.csstransitions,
                columnWidth: 225,
                gutter: 11,
                isFitWidth: true
            });
        });
    }

    initEWNArticleClick(true);

    $('div.sticky, #main-nav').megaMenu();

    $('div.weather-widget').weatherWidget();

    /*boxes = $('.continue li');
    maxHeight = Math.max.apply(
        Math, boxes.map(function() {
            return $(this).height();
        }).get());
    boxes.height(maxHeight);*/

});

function initEWNArticleClick(on){
    var touchMoving = false;
        $(document).off("touchend.ewnarticle, touchmove.ewnarticle");

        $(document).on("touchmove.ewnarticle", function(e)
        {
            touchMoving = true;
        } );

        $(document).on("touchend.ewnarticle", function(e)
        {
            touchMoving = false;
        } );




    var elem = $(".article-short, .home-splash").not('div.sticky .article-short, #main-nav .article-short');
    if (on) {
        elem.off('click.ewnarticle, touchend.ewnarticle').on('click.ewnarticle, touchend.ewnarticle', function(){ // make sure we only have 1 event registered per item
            if (touchMoving){
                touchMoving = false;
                return false;
            }

            window.location= $(this).find("a").attr("href");
            return false;
        });
    } else {
        elem.off('click.ewnarticle, touchend.ewnarticle');
    }
}


function ewnIsIETest(version, comparison) {
    var cc      = 'IE',
        b       = document.createElement('B'),
        docElem = document.documentElement,
        isIE;

    if(version){
        cc += ' ' + version;
        if(comparison){ cc = comparison + ' ' + cc; }
    }

    b.innerHTML = '<!--[if '+ cc +']><b id="iecctest"></b><![endif]-->';
    docElem.appendChild(b);
    isIE = !!document.getElementById('iecctest');
    docElem.removeChild(b);
    return isIE;
}
